import "reflect-metadata";
import * as dotenv from "dotenv";
import { PlaywrightTestConfig, devices } from "@playwright/test";

dotenv.config();

const config: PlaywrightTestConfig = {
  testDir: "tests/e2e/src",
  reporter: "html",
  testMatch: "*",
  use: {
    headless: false,
    baseURL: process.env["BASE_URL"],
  },
  projects: [
    {
      name: "chromium",
      use: {
        ...devices["Desktop Chrome"],
      },
    },
    {
      name: "firefox",
      use: {
        ...devices["Desktop Firefox"],
      },
    },

    {
      name: "webkit",
      use: {
        ...devices["Desktop Safari"],
      },
    },
  ],
  outputDir: "test-results/",
};

export default config;

