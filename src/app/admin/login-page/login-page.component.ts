import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { User } from '../../shared/interfaces';
import { AuthService } from '../shared/services/auth.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: [ './login-page.component.scss' ]
})
export class LoginPageComponent implements OnInit {

  form: UntypedFormGroup | any;
  submitted = false;
  message?: string;

  constructor(
    public auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params: Params) => {
      if ( params['loginAgain'] ) {
        this.message = 'Пожалуйста, введите данные';
      } else if ( params['authFailed'] ) {
        this.message = 'Сессия истекла. Введите данные заново';
      }
    });
    this.form = new UntypedFormGroup({
      email: new UntypedFormControl(null, [
        Validators.email,
        Validators.required
      ]),
      password: new UntypedFormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  submit() {
    if ( this.form.invalid ) {
      return;
    }

    this.submitted = true;

    const user: User = {
      email: this.form.value.email,
      password: this.form.value.password,
      returnSecureToken: this.form.value.returnSecureToken
    };

    this.auth.login(user).subscribe(() => {
      this.form.reset();
      this.router.navigate([ '/admin', 'dashboard' ]).then();
      this.submitted = false;
    }, () => {
      this.submitted = false;
    });
  }
}
