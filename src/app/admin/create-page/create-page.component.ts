import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Post } from '../../shared/interfaces';
import { PostsService } from '../../shared/posts.service';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: [ './create-page.component.scss' ]
})
export class CreatePageComponent implements OnInit {
  form: UntypedFormGroup | any;

  constructor(
    private postService: PostsService,
    private alert: AlertService
  ) {
  }

  ngOnInit(): void {
    this.form = new UntypedFormGroup({
      title: new UntypedFormControl(null, Validators.required),
      text: new UntypedFormControl(null, Validators.required),
      author: new UntypedFormControl(null, Validators.required)
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const post: Post = {
      title: this.form.value.title,
      author: this.form.value.author,
      text: this.form.value.text,
      date: new Date()
    };

    this.postService.create(post).subscribe(() => {
      this.form.reset();
      this.alert.success('Пост был создан');
    });

    console.log(post);
  }

}
