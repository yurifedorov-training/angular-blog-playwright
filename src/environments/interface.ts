export interface Environment {
  apiKey: string | undefined;
  production: boolean;
  fbDbUrl: string | undefined;
}
