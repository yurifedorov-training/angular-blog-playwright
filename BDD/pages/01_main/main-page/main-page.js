import { qase } from "cypress-qase-reporter/dist/mocha";

describe("Main page", () => {

  qase(1,
    it("I see logo on the main page", () => {
      expect(true).to.equal(true);
    })
  );
  qase(2,
    it("I see the main title on the main page", () => {
      expect(true).to.equal(true);
    })
  );
  qase(3,
    it("I see the sign-in button on the main page", () => {
      expect(true).to.equal(true);
    })
  );
});
