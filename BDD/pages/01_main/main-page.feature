Feature: Main page

  Background:
    Given I visit the main page

  Scenario: I see logo on the main page
    When I am on the main page
    Then I see the logo on the main page

  Scenario: I see the main title on the main page
    When test-2 step-1 on the main page
    Then test-2 step-2 on the main page
    
  Scenario: I see the sign-in button on the main page
    When test-3 step-1 on the main page
    Then test-3 step-2 on the main page
