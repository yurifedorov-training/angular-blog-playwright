before(() => {
  When("I am on the sign-in page", () => {
    cy.visit("/admin/login");
  });
});

When("action-1 with email field", () => {
  cy.log("action-1");
});

Then("action-2 with email field", () => {
  cy.log("action-2");
});
