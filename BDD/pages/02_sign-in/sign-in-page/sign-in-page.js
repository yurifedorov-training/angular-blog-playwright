// before(() => {
//   When("I am on the main page", () => {
//     cy.visit("/");
//   });
// });
//
// When("I click to sign-in button", () => {
//   cy.getcy("sign-in-btn").click();
// });
//
// Then("I see login form", () => {
//   cy.getcy("auth-form");
// });
//
// Then("I see form title {string}", (formTitle) => {
//   cy.getcy("auth-form-title").then(($title) => {
//     expect($title).to.have.text(` ${formTitle} `);
//   });
// });
//
// Then("I see form subtitle {string}", (formSubTitle) => {
//   cy.getcy("auth-form-subtitle").then(($subtitle) => {
//     expect($subtitle).to.have.text(` ${formSubTitle} `);
//   });
// });
//
// Then("I fill correct email", () => {
//   cy.getcy("auth-form-email").type(Cypress.env("EMAIL_TEST"));
// });
//
// Then("I fill correct password", () => {
//   cy.getcy("auth-form-password").type(Cypress.env("PASSWORD_TEST"));
// });
//
// Then("I click to submit button", () => {
//   cy.getcy("auth-form-submit").click();
// });
//
// Then("I see page title {string}", (adminPanelTitle) => {
//   cy.getcy("admin-panel-title").then(($title) => {
//     expect($title).to.have.text(`${adminPanelTitle}`);
//   });
// });
//
