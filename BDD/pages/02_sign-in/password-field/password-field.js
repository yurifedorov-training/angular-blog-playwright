before(() => {
  When("I am on the sign-in page", () => {
    cy.visit("/admin/login");
  });
});

When("action-1 with password field", () => {
  cy.log("action-1");
});

Then("action-2 with password field", () => {
  cy.log("action-2");
});
