Feature: Password field validation

  Background:
    Given I am on the sign-in page

  @skip
  Scenario: I fill the password field correctly
    When action-1 with password field
    Then action-2 with password field

  @skip
  Scenario: I didn't fill the password field

  @skip
  Scenario: I fill the password field with min number of symbols allowed + 1

  @skip
  Scenario: I fill the password field with min number of symbols allowed - 1

  @skip
  Scenario: I fill the password field with min number of symbols allowed

  @skip
  Scenario: I fill the password field with max number of symbols allowed + 1

  @skip
  Scenario: I fill the password field with max number of symbols allowed - 1

  @skip
  Scenario: I fill the password field with Latin letters

  @skip
  Scenario: I fill the password field with Cyrillic letters

  @skip
  Scenario: I fill the password field with lower case letters

  @skip
  Scenario: I fill the password field with UPPER case letters

  @skip
  Scenario: I fill the password field with numbers

  @skip
  Scenario: I fill the password field with special characters

  @skip
  Scenario: I fill the password field with spaces at the start and end

  @skip
  Scenario: I fill the email field with spaces in the middle

  @skip
  Scenario: I don't see the password characters when typing

  @skip
  Scenario: I can open and see hidden symbols when clicking on eye icon
