Feature: Email field validation

  Background:
    Given I am on the sign-in page

  @skip
  Scenario: I fill the email field correctly
    When action-1 with email field
    Then action-2 with email field

  @skip
  Scenario: I didn't fill the email field

  @skip
  Scenario: I fill the email field with a max number of symbols allowed + 1

  @skip
  Scenario: I fill the email field with a max number of symbols allowed - 1

  @skip
  Scenario: I fill the email field with Latin letters

  @skip
  Scenario: I fill the email field with Cyrillic letters

  @skip
  Scenario: I fill the email field with hieroglyphs letters

  @skip
  Scenario: I fill the email field with numbers

  @skip
  Scenario: I fill the email field with special characters

  @skip
  Scenario: I fill the email field with spaces at the start and end

  @skip
  Scenario: I fill the email field with spaces in the middle

  @skip
  Scenario: I fill the email field with HTML tags

  @skip
  Scenario: I fill the email field with mnemonic symbols

  @skip
  Scenario: I fill the domain name in the email field

  @skip
  Scenario: I didn't fill in the domain name in the email field

  @skip
  Scenario: I fill in the email field with a single @ character

  @skip
  Scenario: I didn't fill in the email field with @ character

  @skip
  Scenario: I fill in the email field with two @@ character
