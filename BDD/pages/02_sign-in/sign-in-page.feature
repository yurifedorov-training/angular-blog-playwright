Feature: Sign-in page

  Background:
    Given I am on the main page

  Scenario: I open sign-in form
    When I click to sign-in button
    Then I see login form
    Then I see form title "Авторизация"
    Then I see form subtitle "Войдите в панель администратора"
    Then I fill correct email
    Then I fill correct password
    Then I click to submit button
    Then I see page title "Панель администратора"
