import { Page } from "@playwright/test";

// SELECTORS
const HEADER_CONTAINER = "div";
const PRODUCT_LOGO = "data-test-id=logo";
const PRODUCT_NAME = "data-test-id=product-name";
const LOGIN_BUTTON = "data-test-id=sign-in-btn";

export class HeaderPublicComponent {
  private page: Page;
  public constructor(page: Page) {
    this.page = page;
  }

  // COMPONENT ACTIONS
  public async hasProductLogo(): Promise<boolean> {
    await this.page.waitForSelector(PRODUCT_LOGO);
    return await this.page.locator(PRODUCT_LOGO).isVisible();
  }

  public async hasProductName(productName: string): Promise<boolean> {
    return await this.page.locator(PRODUCT_NAME, { hasText: productName }).isVisible();
  }

  public async hasSignInButton(signIn: string): Promise<boolean> {
    return await this.page.locator(LOGIN_BUTTON, { hasText: signIn }).isVisible();
  }
}
