import { Page } from "@playwright/test";
import { HeaderPublicComponent } from "./components/header-public.component";
import { EnvironmentService } from "../../../services/environment.service";
import { container } from "tsyringe";

export class HomePage {
  public header: HeaderPublicComponent;
  private environment: EnvironmentService;
  private readonly page: Page;
  public constructor(page: Page) {
    this.page = page;
    this.environment = container.resolve(EnvironmentService);
    this.header = new HeaderPublicComponent(page);
  }

  public async goto(): Promise<void> {
    await this.page.goto(this.environment.baseUrl);
  }
}
