import { Page } from "@playwright/test";
import { LoginFormComponent } from "./components/login-form.component";
import { container } from "tsyringe";
import { EnvironmentService } from "../../../services/environment.service";

export class AdminLoginPage {
  public loginForm: LoginFormComponent;
  private environment: EnvironmentService;
  private readonly page: Page;
  public constructor(page: Page) {
    this.page = page;
    this.environment = container.resolve(EnvironmentService);
    this.loginForm = new LoginFormComponent(page);
  }

  // ACTIONS
  public async goto(): Promise<void> {
    await this.page.goto(this.environment.baseUrl + "/admin/login");
  }
}
