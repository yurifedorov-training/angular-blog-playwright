import { Page } from "@playwright/test";

// SELECTORS
const ADMIN_LOGIN_FORM = "data-test-id=auth-form";
const EMAIL_FIELD = "data-test-id=auth-form-email";
const PASSWORD_FIELD = "data-test-id=auth-form-password";
const SUBMIT_BUTTON = "data-test-id=auth-form-submit";

export class LoginFormComponent {
  private page: Page;
  public constructor(page: Page) {
    this.page = page;
  }

  // COMPONENT ACTIONS
  public async fillUsername(username: string | undefined): Promise<void> {
    if (typeof username === "string") {
      await this.page.fill(EMAIL_FIELD, username);
    }
  }

  public async fillPassword(password: string | undefined): Promise<void> {
    if (password != null) {
      await this.page.fill(PASSWORD_FIELD, password);
    }
  }

  public async submit(): Promise<void> {
    await this.page.click(SUBMIT_BUTTON);
  }
}
