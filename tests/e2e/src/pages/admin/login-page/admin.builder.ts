class Admin {
  public email: string | undefined;
  public password: string | undefined;
}

export class AdminBuilder {
  private readonly admin: Admin;
  public constructor() {
    this.admin = new Admin();
  }

  public setUserName(username: string): AdminBuilder {
    this.admin.email = username;
    return this;
  }

  public setPassword(password: string): AdminBuilder {
    this.admin.password = password;
    return this;
  }

  public build(): Admin {
    return this.admin;
  }
}
