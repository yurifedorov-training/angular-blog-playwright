import { Page } from "@playwright/test";

// SELECTORS
// FIXME: use class selectors
const PRODUCT_LOGO = "img";
const PRODUCT_NAME = "data-test-id=admin-panel-title";
const LOGIN_BUTTON = "button";


export class HeaderPrivateComponent {
  private page: Page;
  public constructor(page: Page) {
    this.page = page;
  }

  // COMPONENT ACTIONS
  public async hasProductLogo(): Promise<boolean> {
    await this.page.waitForSelector(PRODUCT_LOGO);
    return await this.page.locator(PRODUCT_LOGO).isVisible();
  }

  public async hasPageTitle(pageTitle: string): Promise<boolean> {
    return await this.page.locator(PRODUCT_NAME, { hasText: pageTitle }).isVisible();
  }

}
