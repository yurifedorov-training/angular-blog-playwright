import { Page } from "@playwright/test";
import { HeaderPrivateComponent } from "./components/header-private.component";
import { EnvironmentService } from "../../../services/environment.service";
import { container } from "tsyringe";


export class AdminPanelPage {
  public header: HeaderPrivateComponent;
  private environment: EnvironmentService;
  private readonly page: Page;
  public constructor(page: Page) {
    this.page = page;
    this.environment = container.resolve(EnvironmentService);
    this.header = new HeaderPrivateComponent(page);
  }

  public async goto(): Promise<void> {
    await this.page.goto(this.environment.baseUrl);
  }
}
