interface AuthenticationRequest {
  apiUrl: string;
  apiKey: string;
}

interface AdminAuthenticationRequest extends AuthenticationRequest {
  server?: string;
}

interface AuthenticationResponse {
  access_token: string;
}

abstract class AuthenticationService<TRequest extends AuthenticationRequest = AuthenticationRequest> {
  // TODO: implement class
}

export class AdminAuthenticationService extends AuthenticationService<AdminAuthenticationRequest> {
  // TODO: implement class
}

