import { MemoryStorageKey } from "./memory-storage.data";

export class MemoryStorageService {
  private readonly cache = new Map<MemoryStorageKey, any>();

  public get<T>(key: MemoryStorageKey): T {
    return this.cache.get(key) as T;
  }

  public set<T>(key: MemoryStorageKey, value: T): this {
    this.cache.set(key, value);
    return this;
  }
}
