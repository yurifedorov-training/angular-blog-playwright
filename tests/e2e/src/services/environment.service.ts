export class EnvironmentService {
  public get baseUrl(): any {
    return process.env["BASE_URL"];
  }

  public get apiUrl(): any {
    return process.env["API_URL"]
  }

  public get apiKey(): any {
    return process.env["API_KEY"];
  }

  public get adminEmail(): any {
    return process.env["ADMIN_EMAIL"];
  }

  public get adminPassword(): any {
    return process.env["ADMIN_PASSWORD"];
  }
}
