export class ResourceResult<TResource> {
  // ctor
  private constructor(
    public readonly isSuccess: boolean,
    public readonly data: TResource,
    public readonly error?: Error
  ) {}

  // helpers
  public static success<T>(data: T): ResourceResult<T> {
    return new ResourceResult(true, data);
  }

  public static error<T>(error: Error): ResourceResult<T> {
    return new ResourceResult(false, null as T, error);
  }
}
