import { container } from "tsyringe";
import { Page } from "@playwright/test";
import { AdminLoginPage } from "../pages/admin/login-page/login.page";
import { AdminBuilder } from "../pages/admin/login-page/admin.builder";
import { EnvironmentService } from "./environment.service";

export class CommonActionsService {
  public constructor(private readonly environment: EnvironmentService) {
    this.environment = container.resolve(EnvironmentService);
  }

  public async loginAsAdmin(page: Page): Promise<void> {
    const adminLoginPage = new AdminLoginPage(page);

    await adminLoginPage.goto();
    const admin = new AdminBuilder()
      .setUserName(this.environment.adminEmail)
      .setPassword(this.environment.adminPassword)
      .build();

    // Perform interaction
    const loginForm = adminLoginPage.loginForm;
    await loginForm.fillUsername(admin.email);
    await loginForm.fillPassword(admin.password);
    await loginForm.submit();
  }
}
