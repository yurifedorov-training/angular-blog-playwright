export class StringGeneratorHelper {
  public static generateString(length: number): string | null {
    let result = "";
    let prefix = "autotest_";
    let characters = "0123456789";
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return prefix + result;
  }
}
