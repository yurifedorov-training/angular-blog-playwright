import { test } from '@playwright/test';
import { container } from "tsyringe";
import { AdminAuthenticationService } from "../../services/authentication.service"
import { EnvironmentService } from "../../services/environment.service";
import { HomePage } from "../../pages/public/homepage/home.page";
import { step1 } from './step-1'
import { step2 } from "./step-2";
import { step3 } from "./step-3";

test.describe("Homepage:", () => {
  const environment = container.resolve(EnvironmentService);
  const authentication = container.resolve(AdminAuthenticationService);

  // BACKGROUND
  test.beforeAll(async () => {
    // TODO: add preconditions
  });

  // SCENARIO
  test('System displays the homepage', async ({ page }) => {
    await test.step("Step 1: Find product name", async () => {
      await step1(page);
    });

    await test.step("Step 2: Find product logo", async () => {
      await step2(page);
    });

    await test.step("Step 3: Find 'Sign In' button", async () => {
      await step3(page);
    });
    // TODO: add more steps

  });

})


