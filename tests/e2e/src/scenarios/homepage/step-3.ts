import { assert } from "assertthat";
import { Page } from "@playwright/test";
import { HomePage } from "../../pages/public/homepage/home.page";
import { SignIn } from "../../services/enums";

export async function step3(page: Page): Promise<void> {
  // Given a user opens homepage
  const homepage = new HomePage(page);
  await homepage.goto()

  // The page has title
  const header = homepage.header;
  const hasSignInButton = await header.hasSignInButton(SignIn.SignInText)
  assert.that(hasSignInButton).is.true()
  assert.that(hasSignInButton).is.equalTo(hasSignInButton);

  return;
}
