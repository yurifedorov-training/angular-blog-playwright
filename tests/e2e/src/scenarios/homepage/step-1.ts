import { assert } from "assertthat";
import { Page } from "@playwright/test";
import { HomePage } from "../../pages/public/homepage/home.page";
import { PageTitle } from "../../services/enums";

export async function step1(page: Page): Promise<void> {
  // Given a user opens homepage
  const homepage = new HomePage(page);
  await homepage.goto()

  // The page has title
  const header = homepage.header;
  const hasPageTitle = await header.hasProductName(PageTitle.HomePage);
  assert.that(hasPageTitle).is.equalTo(hasPageTitle);

  return;
}
