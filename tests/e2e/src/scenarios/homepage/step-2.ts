import { assert } from "assertthat";
import { Page } from "@playwright/test";
import { HomePage } from "../../pages/public/homepage/home.page";

export async function step2(page: Page): Promise<void> {
  // Given a user opens homepage
  const homepage = new HomePage(page);
  await homepage.goto()

  // The page has title
  const header = homepage.header;
  const hasLogo = await header.hasProductLogo()
  assert.that(hasLogo).is.true()

  return;
}
