import { assert } from "assertthat";
import { AdminPanelPage } from "../../pages/admin/admin-panel/admin-panel.page";
import { PageTitle } from "../../services/enums";
import {Page} from "@playwright/test";

export async function step1(page: Page): Promise<void> {
  // const commonActions = container.resolve(CommonActionsService);

  // Given a user is logged in as Administrator
  // await commonActions.loginAsAdmin(page);

  // Given a user opens admin panel
  const homepage = new AdminPanelPage(page);
  await homepage.goto()

  // The page has title
  const header = homepage.header;
  const hasPageTitle = await header.hasPageTitle(PageTitle.AdminPage);
  assert.that(hasPageTitle).is.equalTo(hasPageTitle);

  return;
}
